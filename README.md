
<img src='文艺坊图库/世博量化.png' height='100'> <img src='文艺坊图库/大秦赋 - 北京大学.png' height='100'>

<img src='文艺坊图库/alibaba1.jpg' width='240'>

---

[<img src='文艺坊图库/RStudioCloud.png' height='20'>](https://rstudio.cloud) [<img src='文艺坊图库/RStudioCom2.png' height='20'>](https://community.rstudio.com/new-topic?category=shiny&tags=shiny) [![](文艺坊图库/shiny-badge.svg)](https://www.shinyapps.io) 

<img src='文艺坊图库/商场如战场.png' width='560'>

[**主题曲**](https://github.com/englianhu/report/discussions)

🚄东方快车，🚄一带一路。

<audio controls loop autoplay src="文艺坊歌曲库/東方快車合唱團 Oriental Express - 紅紅青春敲呀敲 《黑松沙士》廣告主題曲.mp3" controls></audio>

**大秦赋 (Chinese Emperor)**<br>
春秋战国《*礼记•经解*》<br>
孔子曰：『君子慎始，差若毫厘，缪以千里。』

> <span style='color:#FFEBCD; background-color:#D2B48C;'>**《礼记·经解》孔子曰：**</span><span style='color:#A9A9A9'; background-color:#696969;'>*「君子慎始，差若毫厘，谬以千里。」*</span>[^1]

*引用：[「快懂百科」《礼记•经解》](https://www.baike.com/wikiid/2225522569881832051?view_id=2tt3iw3blkq000)和[第一范文网：差之毫厘，谬以千里的故事](https://www.diyifanwen.com/chengyu/liuziyishangchengyugushi/2010051523105152347092749890.htm)和[「百度百科」春秋时期孔子作品《礼记•经解》](https://baike.baidu.com/item/%E7%A4%BC%E8%AE%B0%C2%B7%E7%BB%8F%E8%A7%A3/2523092)和[「當代中國」差之毫釐 謬以千里](https://www.ourchinastory.com/zh/2962/%E5%B7%AE%E4%B9%8B%E6%AF%AB%E9%87%90%20%E8%AC%AC%E4%BB%A5%E5%8D%83%E9%87%8C)*

[^1]: [HTML Color Codes](https://html-color.codes)

## 量化数学中国共识加油赞

### 不与诈骗Judi巫师邪教集团为伍

> 宁可枝头抱🇹🇼🇨🇳死，<br> (中华民族)
不曾吹落东南风（🇵🇭🇰🇭🇮🇲Judi马来印尼新加坡巫师巫裔邪教Cube Ltd博彩庄家）；<br>
山穷水路疑无🛣，<br>
柳暗花明又一🌸。

> **®Studio 🇺🇸 GI Joe -vs- Kubuntu 🇲🇳 <img src='文艺坊图库/Asean.png' height='12'> Kublai Commander**<br>
**®Studio 🇺🇸 GI Жо -vs- Kubuntu 🇲🇳 <img src='文艺坊图库/Asean.png' height='12'> Хубилай командлагчч**<br>

出处：[猫舍：通过数码海洋（Digital Ocean）建立服务器](https://github.com/scibrokes/setup-centOS7-DO)

> 原本浏览比较了很多博彩网，发现1xbet.com（还🈶API）功能最全面最好 ，原本打算科研后，他日去欧洲发展，不过金融市场好（内地也🈶很多合法著名金融投资基金，他日到大陆发展好），决定不赌。
> 
> 金融市场商品外汇牵涉全球经济，被操控的概率低
外汇商品等金融市场统计建模比较容易
获得连接金融市场API实时数据的平台也比较多和容易

[[咨询]：如何连接到 Web Browser 网页应用的API](https://d.cosx.org/d/421872-web-browser-api/6)

<br><br>

### 经济西征之路

<iframe width="560" height="315" src="https://www.youtube.com/embed/XI4hl9F6wrE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="//player.bilibili.com/player.html?aid=981862346&bvid=BV1Vt4y1s7B3&cid=725422877&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"></iframe>

> Нэг бүс нэг зам: Баруун эдийн засгийн экспедицийн зам~

出处：[[vk] 一带一路：经济西征（考察）之路～](https://vk.com/englianhu?w=wall611842020_104)<br>
出处：[[Tumblr] 一带一路：经济西征（考察）之路～](https://englianhu.tumblr.com/post/684806031267725312/%E4%B8%BA%E4%BB%80%E4%B9%88%E6%B8%B8%E6%88%8F%E5%BC%80%E5%8F%91%E5%95%86%E5%BE%88%E5%B0%91%E5%BC%80%E5%8F%91%E4%B8%AD%E5%9B%BD%E5%8E%86%E5%8F%B2%E4%B8%8A%E7%9A%84%E5%85%B6%E4%BB%96%E6%97%B6%E6%9C%9F-%E7%9F%A5%E4%B9%8E)

<br>

## 乐发集团（东方集团）

- 东方集团

  - 乐发集团
    - 阿里
    - 一号彩
    
### 阿里报告

- 阿里报告
- 彩种报告
- 会员报表

### 闪霓应用

一键生成报告。

<br>

## 中国手机号

- [中国号，心头好 (RPubs)](https://rpubs.com/englianhu/845478)
- [中国号，心头好 (RStudioConnect)](https://beta.rstudioconnect.com/connect/#/apps/7dd0a6c7-defc-4c82-9c99-8788bb95d7b0/access)

<br>

## 参考文献

- [Radiant - A shiny app for statistics and machine learning](https://shiny.rstudio.com/gallery/radiant.html) 🔥
- [Radiant – Business analytics using R and Shiny](https://radiant-rstats.github.io/docs/install.html)
- [Research & Development Business Game](https://shiny.rstudio.com/gallery/rd-business-game.html)
- [New Zealand Trade Intelligence Dashboard](https://shiny.rstudio.com/gallery/nz-trade-dash.html)
- [ScotPHO Profiles Tool](https://shiny.rstudio.com/gallery/scotpho-profiles.html)
- [FIFA'19 Analysis by Nationality](https://demo.appsilon.com/apps/fifa19/#!/country)
- [Material Design + AdminLTE](https://ducthanhnguyen.github.io/MaterialAdminLTE/index3.html) 🔥
- [Improved Skins](https://rinterface.github.io/shinydashboardPlus/articles/more-skins.html)
- [Making a Shiny dashboard using `highcharter` – Analyzing Inflation Rates](https://datascienceplus.com/making-a-shiny-dashboard-using-highcharter-analyzing-inflation-rates)
- [DATA ANALYSIS](https://rpubs.com/Author_Nasila18/836843)
- [永洪一站式大数据分析平台](https://www.yonghongtech.com) ([Demo体验](http://public.yonghongtech.com/bi/?au_act=login&adminv=demo&passv=Yonghong123), [企业试用版](https://www.yonghongtech.com/al/zhuce/index.html?module=shiyong&column=))
- [DATA ANALYSIS](https://rpubs.com/Author_Nasila18/836843)
- [R语言中文网“apriori” 相关内容](https://www.r-china.net/search.php?mod=forum&searchid=23&orderby=lastpost&ascdesc=desc&searchsubmit=yes&kw=apriori)
- [用朴素贝叶斯模型预测柯南中被害人和凶手！](https://gist.github.com/baymaxium/0357d4e9b1d365475659f7f55f851150)
- [如何用简单易懂的例子解释隐马尔可夫模型？](https://www.zhihu.com/question/20962240)
- [R语言估计时变VAR模型时间序列的实证研究分析案例](http://tecdat.cn/r%e8%af%ad%e8%a8%80%e4%bc%b0%e8%ae%a1%e6%97%b6%e5%8f%98var%e6%a8%a1%e5%9e%8b%e6%97%b6%e9%97%b4%e5%ba%8f%e5%88%97%e7%9a%84%e5%ae%9e%e8%af%81%e7%a0%94%e7%a9%b6%e5%88%86%e6%9e%90%e6%a1%88%e4%be%8b)

---

[<img src="文艺坊图库/Scibrokes.png" height="14"/> Sςιβrοκεrs Trαdιηg®](http://www.scibrokes.com)<br>
<span style='color:RoyalBlue'>**[<img src="文艺坊图库/Scibrokes.png" height="14"/> 世博量化®](http://www.scibrokes.com)企业知识产权及版权所有，盗版必究。**</span>
